# SUM PROBLEM



## Getting started

Given a list of n integers, return the two numbers such that their absolute value added together can produce the greatest sum.
The values of each element of the array will be in the range of  [-2,147,483,648 to 2,147,483,647]
The number of elements in the input values will be in the range of [2 to 1000000]
The results must be sorted.

| Input | Output
| :----- | :------
| [1, 2, 3, 0] | [2, 3]
| [4, 3, 9, 1, 2, 5, 8, 0] | [8, 9]

You must implement the solution in the function
```
int[] sum(int n[])
{
}
```

You can test the solution running the code.
Once you have finished create a commit and push the changes.
