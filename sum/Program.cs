﻿// See https://aka.ms/new-console-template for more information
using sum;

var s = new Solution();
Console.WriteLine(string.Join(",", s.sum(new int[] { 1, 2, 3, 0 })));
Console.WriteLine(string.Join(",", s.sum(new int[] { 4, 3, 9, 1, 2, 5, 8, 0 })));
