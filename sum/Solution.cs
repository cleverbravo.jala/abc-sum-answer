﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sum
{
    public class Solution
    {
        public int[] sum(int[] n)
        {
            var list = n.ToList();
            list.Sort();
            list.Reverse();
            return new int[] { list[1], list[0] };
        }
    }
}